const Ajv = require('ajv');
const amqplib = require('amqplib');

class Processing {
    /**
     * Конструктор
     */
    constructor(options) {
        this.__hostname = options && options.hostname ? options.hostname : 'localhost';//Адрес сервера
		this.__port = options && options.port ? options.port : 5672;//Порт сервера
		this.__username = options && options.username ? options.username : 'guest';//Логин подключения
		this.__password = options && options.password ? options.password : 'guest';//Пароль подключения
		this.__heartbeat = options && options.heartbeat ? options.heartbeat : 30;//Проверка соединения
		this.__frameMax = options && options.frameMax ? options.frameMax : 0;//Размер фрейма
		this.__locale = options && options.locale ? options.locale : 'en_US';//Язык по умолчанию
        this.__vhost = options && options.vhost ? options.vhost : '/';//Путь до экземпляра
		this.__prefetch = options && options.prefetch && typeof(options.prefetch) == 'number' ? options.prefetch : 1;
		this.__reconnect = options && options.reconnect && typeof(options.reconnect) == 'boolean' ? options.reconnect : false;//Переподключаться, если был разрыв соединения
        this.__reconnectTimeout = options && options.reconnectTimeout && typeof(options.reconnectTimeout) == "number" ? options.reconnectTimeout * 1000 : 30000;//Если установлен флаг переподключения, то выдержать таймаут перед переподключением
        this.__countMsg = options && options.countMsg && typeof(options.countMsg) == 'number' ? options.countMsg : 1000;//Минимальный размер буферов, при котором будут отправлены пачки
        this.__timeSend = options && options.timeSend && typeof(options.timeSend) == 'number' ? options.timeSend * 1000 : 1000;//Периодичность срабатывания отпаврки буферов в не зависимости от того заполнены ли они полностью или нет
        this.__bufferOutgoing = [];//Буффер обработанных сообщений
        this.__bufferNotProceed = [];//Буффер не обработанных сообщений по каким-либо причинам
        this.__bufferError = [];//Буффер ошибок

		this.__connection = null;
        this.__channel = null;
        
        this.__activated = false;
        this.__onConnected = false;

        this.__fayulogger = null;

        this.__queueIncoming = null;//Очередь входящих сообщений
        this.__queueOutgoing = null;//Очередь обработанных сообщений
        this.__queueNotProceed = null;//Очередь не обработанных сообщений
        this.__queueError = null;//Очередь ошибок
        this.__func = null;//Функция обработки. Если функция не указана, то происходит простое "перекладываение" сообщений из очереде incoming в outgoing, либо в очередь error (если указана схема валидации)
        this.__schema = null;//Схема валидации JSONSchema
        this.__ajv = new Ajv(require('ajv/lib/refs/json-schema-draft-07.json'))

        this.__timeInterval = null;//идентификатор интервала отправки
    }


    bind(options) {
        if(options && options.incoming) {
            this.__queueIncoming = options.incoming.queue;
            this.__func = options.incoming.func;
        }
        //this.__queueIncoming = options && options.incoming ? options.incoming : null;
        this.__queueOutgoing = options && options.outgoing ? options.outgoing : null;
        this.__queueNotProceed = options && options.notProceed ? options.notProceed : null;
        this.__queueError = options && options.error ? options.error : null;
        //this.__func = options && options.func ? this.__func : null;
        if(options.schema != null) {
            this.__schema = this.__ajv.compile(options.schema)
        } else {
            this.__schema = null;
        }
    }

    /**
	 * Подключение к серверу
	 */
	async __connect() {
		try {
			//Подключение к MQ
			this.__connection = await amqplib.connect({
				protocol: 'amqp',
				hostname: this.__hostname,
				port: this.__port,
				username: this.__username,
				password: this.__password,
				locale: this.__locale,
				frameMax: this.__frameMax,
				heartbeat: this.__heartbeat,
				vhost: this.__vhost
			});
			this.__sendLog('info', {
				protocol: 'amqp',
				hostname: this.__hostname,
				port: this.__port,
				username: this.__username,
				locale: this.__locale,
				frameMax: this.__frameMax,
				heartbeat: this.__heartbeat,
				vhost: this.__vhost
			})
		}catch(e){
			if(this.__reconnect) {
				this.__reconnectAgain();
			} else {
				this.stop();
			}
			this.__sendLog("error", {
				errName: e.name,
				strack: e.stack
			})
			throw e;
		}
    }

    /**
     * Отправка буферов в заданные очереди
     */
    __sender() {
        if(this.__activated && this.__onConnected) {
            //Отправляем данные 
            if(this.__queueOutgoing != null) {
                if(this.__bufferOutgoing.length > 0) {
                    let buff = this.__bufferOutgoing;
                    this.__bufferOutgoing = [];
                    this.__channel.sendToQueue(this.__queueOutgoing, Buffer.from(JSON.stringify(buff)));
                }
            }
            if(this.__queueNotProceed != null) {
                if(this.__bufferNotProceed.length > 0) {
                    let buff = this.__bufferNotProceed;
                    this.__bufferNotProceed = [];
                    this.__channel.sendToQueue(this.__queueNotProceed, Buffer.from(JSON.stringify(buff)));
                }
            }
            if(this.__queueError != null) {
                if(this.__bufferError.length > 0) {
                    let buff = this.__bufferError;
                    this.__bufferError = [];
                    this.__channel.sendToQueue(this.__queueError, Buffer.from(JSON.stringify(buff)));
                }
            }
        }
    }

    /**
     * Данные обработаны корректно и могут быть помещены в буфер для передачи далее по цепочке
     * @param {*} res Данные
     */
    outgoing(res) {
        if(this.__activated) {
            if(this.__queueOutgoing != null) {
                this.__bufferOutgoing.push(res);
                if(this.__bufferOutgoing.length >= this.__countMsg) {
                    this.__sender();
                }
            }
        }
    }

    /**
     * Добавление в буфер данных, которые не обработаны функией по решению самой функции
     * @param {*} res Данные
     */
    notProceed(res) {
        if(this.__activated) {
            if(this.__queueNotProceed != null) {
                this.__bufferNotProceed.push(res);
                if(this.__bufferNotProceed.length >= this.__countMsg) {
                    this.__sender();
                }
            }
        }
    }

    /**
     * Добавление данные с ошибкой в буфер для дальнейшей передачи
     * @param {Object} err Объект ошибки
     * @param {*} res Данные
     */
    error(err, res) {
        if(this.__activated) {
            if(this.__queueError != null) {
                this.__bufferError.push({err: err, res: res});
                if(this.__bufferError.length >= this.__countMsg) {
                    this.__sender();
                }
            }
        }
    }



    /**
     * Подготовка очередей и привязка
     */
    async __prepare() {
        try {
			//Создание канала в MQ
			this.__channel = await this.__connection.createChannel();
			this.__channel.prefetch(this.__prefetch);
			this.__channel.on('close', () => {
                //если канал закрыт
                this.__onConnected = false;
				this.__reconnectAgain();
			});
			this.__channel.on('error', (err) => {
				console.error(err);
				//TODO если получена ошибка канала
			});
			this.__channel.on('return', (msg) => {
				//console.log(msg.content);
				//TODO если возвращено сообщение, которое не удалось отправить в очередь
			});
			this.__channel.on('drain', () => {
				//TODO Like a stream.Writable, a channel will emit 'drain', if it has previously returned false from #publish or #sendToQueue, once its write buffer has been emptied (i.e., once it is ready for writes again).
			});
			this.__sendLog("info", "Channel created");
		}catch(e){
			this.stop();
			this.__sendLog("error", {
				errName: e.name,
				strack: e.stack
			})
			throw e;
        }
        //Входящие сообщения
        try {
            if(this.__queueIncoming != null) {
                await this.__channel.assertQueue(this.__queueIncoming, {
                    /*exclusive: true,*/
                    autoDelete: false,
                    durable: true
                })

            }
        } catch(e){
            this.stop();
            this.__sendLog("error", {
				errName: e.name,
				strack: e.stack
			})
            throw e;
        }
        if(this.__queueIncoming) {
            try {
                await this.__channel.consume(this.__queueIncoming, (msg) => {
                    let content = JSON.parse(msg.content);
                    if(content instanceof Array) {
                        //Массив. Обработаем каждое сообщение в массиве
                        for(let i = 0; i < content.length; i++) {
                            if(this.__schema != null) {
                                if(this.__schema(content[i])) {
                                    //this.__func(msg.msg[i], this.outgoing, this.__notProceed, this.__error);
                                    if(typeof(this.__func) == 'function') {
                                        this.__func(content[i]);
                                    } else {
                                        this.outgoing(content[i]);
                                    }
                                } else {
                                    this.error(this.__schema.errors, content[i]);
                                }
                            } else {
                                if(typeof(this.__func) == 'function') {
                                    this.__func(content[i]);
                                } else {
                                    this.outgoing(content[i]);
                                }
                            }
                        }
                    }
                    //Подтверждаем сообщение
                    this.__channel.ack(msg);
                })
            } catch(e) {
                this.stop();
                this.__sendLog("error", {
                    errName: e.name,
                    strack: e.stack
                })
                throw e;
            }
        }
        /*//Обработанные сообщения
        try {
            if(this.__queueOutgoing != null) {
                await this.__channel.assertQueue(this.__queueOutgoing, {
                    //exclusive: true,
                    autoDelete: false,
                    durable: true
                })

            }
        } catch(e){
            this.stop();
            this.__sendLog("error", {
				errName: e.name,
				strack: e.stack
			})
            throw e;
        }
        //Не обработанные сообщения
        try {
            if(this.__queueNotProceed != null) {
                await this.__channel.assertQueue(this.__queueNotProceed, {
                    //exclusive: true,
                    autoDelete: false,
                    durable: true
                })

            }
        } catch(e){
            this.stop();
            this.__sendLog("error", {
				errName: e.name,
				strack: e.stack
			})
            throw e;
        }
        //Ошибочные сообщения
        try {
            if(this.__queueError != null) {
                await this.__channel.assertQueue(this.__queueError, {
                    //exclusive: true,
                    autoDelete: false,
                    durable: true
                })

            }
        } catch(e){
            this.stop();
            this.__sendLog("error", {
				errName: e.name,
				strack: e.stack
			})
            throw e;
        }*/
    }



    /**
     * Запуск сервера
     */
    async run() {
        this.__activated = true;
        await this.__connect();
        await this.__prepare();
        this.__timeInterval = setInterval(() => {
            this.__sender()
        }, this.__timeSend);
        this.__onConnected = true;
    }

    /**
     * Остановка сервера обработки
     */
    async stop() {
        clearInterval(this.__timeInterval);
        this.__activated = false;
        this.__onConnected = false;
        this.__channel = null;
		try {
			if(this.__connection != null) {
				this.__connection.close();
			}
		}finally{
			this.__connection = null;
        }
        return true;

    }

    /**
	 * Повторно запускаем переподключение к серверу
	 */
	__reconnectAgain() {
		if(this.__activated && this.__reconnect) {
			setTimeout(() => {
				(async () => {
					this.run()
				})().then();
			}, this.__reconnectTimeout);
			//TODO Соединение было установлено, запускаем реконнект к серверу
		}
    }

    /**
	 * Отправка сообщения лога в систему логирования (если установлена)
	 * @param {String} level Уровень логирования
	 * @param {Object} msg Сообщение
	 */
	__sendLog(level, msg) {
		if(this.__fayulogger) {
			switch(level.toLowerCase()) {
				case 'debug':
					for(let mod in this.__fayulogger.modules) {
						mod.debug(msg);
					}
					break;
				case 'info':
					for(let mod in this.__fayulogger.modules) {
						mod.info(msg);
					}
					break;
				case 'warn':
					for(let mod in this.__fayulogger.modules) {
						mod.warn(msg);
					}
					break;
				case 'severe':
					for(let mod in this.__fayulogger.modules) {
						mod.severe(msg);
					}
					break;
				case 'error':
					for(let mod in this.__fayulogger.modules) {
						mod.error(msg);
					}
					break;
				case 'fatal':
					for(let mod in this.__fayulogger.modules) {
						mod.fatal(msg);
					}
					break;
			}
		}
    }
    set FAYULogger(value) {
		this.__fayulogger = value;
	}

	get FAYULogger() {
		return this.__fayulogger;
    }
}

module.exports = Processing;