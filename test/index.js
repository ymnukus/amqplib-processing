const Processing = require('../Processing');

let generate = new Processing({reconnect: true, reconnectTimeout: 3});
generate.bind({
    outgoing: 'outgoing'
})
generate.run();

let justStream = new Processing({reconnect: true, reconnectTimeout: 3});
justStream.bind({
    incoming: {queue:'outgoing'},
    outgoing: 'outgoingStream'
})
justStream.run();

let incomingFromStream = new Processing({reconnect: true, reconnectTimeout: 3});
incomingFromStream.bind({
    incoming: {queue:'outgoingStream', func: function(msg) {
        if(msg % 5 === 0) {
            incomingFromStream.notProceed(msg);
        }
        if(msg % 3 === 0) {
            incomingFromStream.error('test err', msg);
        }
        //console.log(`Incoming: ${msg}`);
    }},
    outgoing: 'outgoingStream',
    notProceed: 'notProceed',
    error: 'error1'
})
incomingFromStream.run();

let incNotProceed = new Processing({reconnect: true, reconnectTimeout: 3});
incNotProceed.bind({
    incoming: {queue: 'notProceed', func: function(msg) {
        console.log(`Not Proceed: ${msg}`);
    }}
})
incNotProceed.run();

let incerr = new Processing({reconnect: true, reconnectTimeout: 3});
incerr.bind({
    incoming: {queue: 'error1', func: function(msg) {
        console.log(`error: ${JSON.stringify(msg)}`);
    }}
})
incerr.run();

let num = 0;
setInterval(() => {
    num++;
    generate.outgoing(num);
    //console.log(num);
}, 50)